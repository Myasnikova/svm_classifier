#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

function categ_mean () {

out=../../data_categ_outSVM/out_"$1"
mean=$(grep Accuracy ${out} | awk '{print $3}' | sed 's/\%//' | awk '{ total += $1 } END { print total/NR }')
bl=baseline_"$1".txt
echo ""
echo -e "${GREEN}Maen: ${NC}"
echo ${mean}
echo ""
echo ${mean} > ${bl}

}

categ_mean "kn"
categ_mean "kl"
categ_mean "ln"

