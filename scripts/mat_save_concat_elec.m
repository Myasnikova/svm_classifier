function mat_save_concat_elec(from,to,name,seed)

cd P:\GCFD\matlab_SPoC
%cd /home/sasha/Desktop/GCFD/matlab_SPoC
startup_spoc
cd P:\GCFD\gcfd
%cd /home/sasha/Desktop/GCFD/gcfd
startup_gcfd
if strcmp(name,'ATANOVMS')
    front_categ(conf(),from,to,{'permut_seed',seed,'save_concat_elec','true'})
else
    front_categ(conf(),from,to,{'Name',name,'permut_seed',seed,'save_concat_elec','true'})
end
exit

