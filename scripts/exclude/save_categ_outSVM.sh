#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

source ../Names.config

function run_easy () {

data_dir=../../data_categ_inSVM/"$1"/"$2"
out=../../data_categ_outSVM/out_"$2"_"$1"

cur_dir=$PWD
cd /home/sasha/Desktop/GCFD/libsvm/tools

for ((i=1; i<=100; i++))
do
    echo "run ($1, $2) #${i}"
    echo "run #${i}" >> ${out}
    train_name=${data_dir}/pattern_train_${i}
    test_name=${data_dir}/pattern_test_0
    ./easy.py ${train_name} ${test_name} >> ${out}
    echo -e "${GREEN}Run PASS${NC}"
    echo ""
    echo "" >> ${out}
done

cd ${cur_dir}

}

out_dir=../../../data_categ_outSVM
echo "cleanup ..."
echo "rm -rf ${out_dir}"
rm -rf ${out_dir}
echo "mkdir ${out_dir}"
mkdir ${out_dir}
echo "DONE"
echo ""

for currName in "${Name[@]}"
do
    run_easy "kn" ${currName}
    run_easy "kl" ${currName}
    run_easy "ln" ${currName}
done
