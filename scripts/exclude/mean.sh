#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

source ../Names.config

function categ_mean () {

out=../../../data_categ_outSVM/out_"$2"_"$1"
mean=$(grep Accuracy ${out} | awk '{print $3}' | sed 's/\%//' | awk '{ total += $1 } END { print total/NR }')
bl=baseline_"$1".txt
echo ""
echo -e "${GREEN}Mean ($1, $2): ${NC}"
echo ${mean}
echo ""
echo "${mean} - $2" >> ${bl}

}

rm -rf baseline*

for currName in "${Name[@]}"
do
    categ_mean "kn" ${currName}
    categ_mean "kl" ${currName}
    categ_mean "ln" ${currName}
done
