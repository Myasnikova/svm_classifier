#!/bin/bash

source ../Names.config
data_dir=../../../data_categ_inSVM
create_dirs () {
    cur_dir=${data_dir}/$1
    mkdir ${cur_dir}
    for currName in "${Name[@]}"
    do
        mkdir ${cur_dir}/${currName}
    done
}

echo "cleanup data dir ..."
echo "rm -rf ${data_dir}"
rm -rf ${data_dir}
echo "mkdir ${data_dir}"
mkdir ${data_dir}
echo "DONE"
echo ""

echo "create dirs ..."
create_dirs "kn"
create_dirs "ln"
create_dirs "kl"
echo "DONE"
echo ""

echo "data prep ..."
for currName in "${Name[@]}"
do
    echo "/usr/local/MATLAB/R2018a/bin/matlab -nodisplay -nodesktop -r \"mat_gen_data_exclude('${currName}')\""
    /usr/local/MATLAB/R2018a/bin/matlab -nodisplay -nodesktop -r "mat_gen_data_exclude('${currName}')"
done
echo "DONE"
echo ""

