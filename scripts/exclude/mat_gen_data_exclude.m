function mat_gen_data_exclude(name)

cd /home/sasha/Desktop/GCFD/svm_classifier
startup

data_gen_exclude('kn',1,100,name,'train')
data_prep_exclude('kn',0,name,'test')

data_gen_exclude('ln',1,100,name,'train')
data_prep_exclude('ln',0,name,'test')

data_gen_exclude('kl',1,100,name,'train')
data_prep_exclude('kl',0,name,'test')

exit
