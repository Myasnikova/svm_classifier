#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

src_dir=../../../data_categ_res
data_dir=../../../data_categ_aggr

source ../Names.config

save_categ_aggr_kln () {

excludeName=$2
exclude=$3

if [ $1 = "k" ];
then
    declare -a Pair=(0)
    echo "Your choice is k"
elif [ $1 = "l" ];
then
    declare -a Pair=(1)
    echo "Your choice is l"
else [ $1 = "n" ];
    declare -a Pair=(2)
    echo "Your choice is n"
fi
declare -a From=("1" "51" "101")
declare -a To=("50" "100" "150")
declare -a Seed=("0" "1" "2")
declare -a FrBase=("4" "5" "6")
test_no=1
exist_test_no=1
empty_test_no=1
pval_line_no=1
check_line_no=1
for currPair in "${Pair[@]}"
do
    currFrom="${From[${currPair}]}"
    currTo="${To[${currPair}]}"
    if [[ ${exclude} -ne 0 ]]
    then
    for currName in "${Name[@]}"
    do
        if [ ${currName} == ${excludeName} ]
        then
            continue
        fi
        for currSeed in "${Seed[@]}"
        do
            for currFrBase in "${FrBase[@]}"
            do
                echo "file #${test_no}"

                pval_p=${src_dir}/res_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}"_FrBase_"${currFrBase}".pval_p
                pval_q=${src_dir}/res_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}"_FrBase_"${currFrBase}".pval_q
                pattern_p=${src_dir}/res_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}"_FrBase_"${currFrBase}".pattern_p
                pattern_q=${src_dir}/res_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}"_FrBase_"${currFrBase}".pattern_q
                kuramoto=${src_dir}/res_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}"_FrBase_"${currFrBase}".ku
                echo "${pval_p}"
                pval_line_no=$(cat ${pval_p} | wc -l)

                check_line_no=$(cat ${pval_q} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pval_q}"
                    exit
                fi

                check_line_no=$(cat ${pattern_p} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pattern_p}"
                    exit
                fi

                check_line_no=$(cat ${pattern_q} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pattern_q}"
                    exit
                fi

                check_line_no=$(cat ${kuramoto} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${kuramoto}"
                    exit
                fi

                if [ -f "${pval_p}" ];
                then
                    if [ ! -s "${pval_p}" ];
                    then
                        echo -e "${RED}File is empty${NC}"
                        let "empty_test_no=empty_test_no+1"
                    fi
                    cat ${pattern_p} >> ${data_dir}/${excludeName}/"$1"_train_pattern_p.res
                    cat ${pval_p} >> ${data_dir}/${excludeName}/"$1"_train_pval_p.res
                    cat ${pattern_q} >> ${data_dir}/${excludeName}/"$1"_train_pattern_q.res
                    cat ${pval_q} >> ${data_dir}/${excludeName}/"$1"_train_pval_q.res
                    cat ${kuramoto} >> ${data_dir}/${excludeName}/"$1"_train_kuramoto.res
                    echo -e "${GREEN}OK${NC}"

                    let "exist_test_no=exist_test_no+1"
                else
                    echo -e "${RED}File does not exist${NC}"
                fi
                echo ""

                let "test_no=test_no+1"
            done
        done
    done
    else
    for currName in "${Name[@]}"
    do
        if [ ${currName} == ${excludeName} ]
        then
        for currSeed in "${Seed[@]}"
        do
            for currFrBase in "${FrBase[@]}"
            do
                echo "file #${test_no}"

                pval_p=${src_dir}/res_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}"_FrBase_"${currFrBase}".pval_p
                pval_q=${src_dir}/res_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}"_FrBase_"${currFrBase}".pval_q
                pattern_p=${src_dir}/res_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}"_FrBase_"${currFrBase}".pattern_p
                pattern_q=${src_dir}/res_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}"_FrBase_"${currFrBase}".pattern_q
                kuramoto=${src_dir}/res_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}"_FrBase_"${currFrBase}".ku
                echo "${pval_p}"
                pval_line_no=$(cat ${pval_p} | wc -l)

                check_line_no=$(cat ${pval_q} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pval_q}"
                    exit
                fi

                check_line_no=$(cat ${pattern_p} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pattern_p}"
                    exit
                fi

                check_line_no=$(cat ${pattern_q} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pattern_q}"
                    exit
                fi

                check_line_no=$(cat ${kuramoto} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${kuramoto}"
                    exit
                fi

                if [ -f "${pval_p}" ];
                then
                    if [ ! -s "${pval_p}" ];
                    then
                        echo -e "${RED}File is empty${NC}"
                        let "empty_test_no=empty_test_no+1"
                    fi
                    cat ${pattern_p} >> ${data_dir}/${excludeName}/"$1"_test_pattern_p.res
                    cat ${pval_p} >> ${data_dir}/${excludeName}/"$1"_test_pval_p.res
                    cat ${pattern_q} >> ${data_dir}/${excludeName}/"$1"_test_pattern_q.res
                    cat ${pval_q} >> ${data_dir}/${excludeName}/"$1"_test_pval_q.res
                    cat ${kuramoto} >> ${data_dir}/${excludeName}/"$1"_test_kuramoto.res
                    echo -e "${GREEN}OK${NC}"

                    let "exist_test_no=exist_test_no+1"
                else
                    echo -e "${RED}File does not exist${NC}"
                fi
                echo ""

                let "test_no=test_no+1"
            done
        done
        fi
    done
    fi
done

echo "empty_test_no: ${empty_test_no}, exist_test_no: ${exist_test_no}"

}

echo "cleanup data dir ..."
echo "rm -rf ${data_dir}"
rm -rf ${data_dir}
echo "mkdir ${data_dir}"
mkdir ${data_dir}
echo "DONE"
echo ""

for exclude_name in "${Name[@]}"
do
    mkdir ${data_dir}/${exclude_name}
    save_categ_aggr_kln "k" ${exclude_name} 1
    save_categ_aggr_kln "k" ${exclude_name} 0
    save_categ_aggr_kln "l" ${exclude_name} 1
    save_categ_aggr_kln "l" ${exclude_name} 0
    save_categ_aggr_kln "n" ${exclude_name} 1
    save_categ_aggr_kln "n" ${exclude_name} 0
done

