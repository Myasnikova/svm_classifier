#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

data_dir=../../data_categ_concat

source Names.config

#echo "cleanup data dir ..."
#rm -rf ${data_dir}/*
#echo "rm -rf ${data_dir}/*"
#echo "DONE"
#echo ""

declare -a Pair=(0 1 2)
declare -a From=("1" "51" "101")
declare -a To=("50" "100" "150")
declare -a Seed=("0" "1" "2")
test_no=1
for currPair in "${Pair[@]}"
do
    currFrom="${From[${currPair}]}"
    currTo="${To[${currPair}]}"
    for currName in "${Name[@]}"
    do
        for currSeed in "${Seed[@]}"
        do
            echo "run #${test_no} mat_save_concat_elec(${currFrom}, ${currTo}, ${currName}, ${currSeed})"

            concat_elec_out=${data_dir}/concat_elec_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}".elec
            echo "${concat_elec_out}"

            if [ ! -f "${concat_elec_out}" ];
            then
                echo "run MATLAB script ..."
                echo "/usr/local/MATLAB/R2018a/bin/matlab -nodisplay -nodesktop -r \"mat_save_concat_elec(${currFrom},${currTo},'${currName}','${currSeed}')\""
                /usr/local/MATLAB/R2018a/bin/matlab -nodisplay -nodesktop -r "mat_save_concat_elec(${currFrom},${currTo},'${currName}','${currSeed}')"
                echo "DONE"
            fi

            echo -e "${GREEN}Run PASS${NC}"
            echo ""

            let "test_no=test_no+1"
        done
    done
done
exit


