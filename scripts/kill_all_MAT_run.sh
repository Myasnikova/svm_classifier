#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

p=$( ps -x | grep MAT | grep -v grep | awk '{print $1}' )
k=$( echo ${p} | awk '{print $1}' )
echo -e "${RED}Kill MAT...(no: ${k})${NC}"
kill -9 ${k}
echo -e "${GREEN}Done${NC}"

p=$( ps -x | grep bash.*run | grep -v grep | awk '{print $1}' )
k=$( echo ${p} | awk '{print $1}' )
echo -e "${RED}Kill run...(no: ${k})${NC}"
kill -9 ${k}
echo -e "${GREEN}Done${NC}"

