#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

function run_easy () {

data_dir=../../data_categ_inSVM/"$1"
out=../../data_categ_outSVM/out_"$1"
rm ${out}

cur_dir=$PWD
cd /home/sasha/Desktop/GCFD/libsvm/tools

for ((i=1; i<=100; i++))
do
    echo "run #${i}"
    echo "run #${i}" >> ${out}
    train_name=${data_dir}/pattern_train_${i}
    test_name=${data_dir}/pattern_test_${i}
    ./easy.py ${train_name} ${test_name} >> ${out}
    echo -e "${GREEN}Run PASS${NC}"
    echo ""
    echo "" >> ${out}
done

cd ${cur_dir}

}

run_easy "kn"
run_easy "kl"
run_easy "ln"

