function mat_save_pat_pval(from,to,name,seed,FrBase)

cd /home/sasha/Desktop/GCFD/matlab_SPoC
startup_spoc
cd /home/sasha/Desktop/GCFD/gcfd
startup_gcfd
if strcmp(name,'ATANOVMS')
    back_categ(algo(front_categ(conf({'permute_elec','true'}),from,to,{'permut_seed',seed,'load_concat_elec','true','FrBase',FrBase})))
%    back_categ(algo(front_categ(conf(),from,to,{'permut_seed',seed,'load_concat_elec','true','FrBase',FrBase})))
else
    back_categ(algo(front_categ(conf({'permute_elec','true'}),from,to,{'Name',name,'permut_seed',seed,'load_concat_elec','true','FrBase',FrBase})))
%    back_categ(algo(front_categ(conf(),from,to,{'Name',name,'permut_seed',seed,'load_concat_elec','true','FrBase',FrBase})))
end
exit

