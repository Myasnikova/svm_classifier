#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

#src_dir=../../../data_coma_res
data_dir=../../../data_coma_sim_separate_num4_aggr
src_dir=../../../data_coma_sim_separate_theta_alpha_res

save_coma_aggr_separ () {

if [[ "$1" = "plus" && "$2" = "sim" ]];
then 
    source Names_plus.config
    echo "Your choice is coma plus sim"
elif [[ "$1" = "plus" && "$2" = "fon" ]];
then 
    source Names_plus.config
    echo "Your choice is coma plus fon"
elif [[ "$1" = "minus" && "$2" = "sim" ]];
then
    source Names_minus.config
    echo "Your choice is coma minus sim"
else
    source Names_minus.config
    echo "Your choice is coma minus fon"    
fi

declare -a Group=($1)
declare -a Cat=($2)
#declare -a Seed=("0" "1" "2")
declare -a Seed=("0")
declare -a FrBase=("4" "5" "6")
declare -a Num=("4")

test_no=1
exist_test_no=1
empty_test_no=1
pval_line_no=1
check_line_no=1

for currGroup in "${Group[@]}"
do
    for currCat in "${Cat[@]}"
    do
        for currName in "${Name[@]}"
        do
            for currSeed in "${Seed[@]}"
            do
                for currFrBase in "${FrBase[@]}"
                do
                    for currNum in "${Num[@]}"
                    do
                        echo "file #${test_no}"

                        pval_p=${src_dir}/res_"${currName}"_type_"${currCat}"_seed_"${currSeed}"_num_"${currNum}"_FrBase_"${currFrBase}"_num_"${currNum}".pval_p
                        pval_q=${src_dir}/res_"${currName}"_type_"${currCat}"_seed_"${currSeed}"_num_"${currNum}"_FrBase_"${currFrBase}"_num_"${currNum}".pval_q
                
                        pattern_p=${src_dir}/res_"${currName}"_type_"${currCat}"_seed_"${currSeed}"_num_"${currNum}"_FrBase_"${currFrBase}"_num_"${currNum}".pattern_p
                        pattern_q=${src_dir}/res_"${currName}"_type_"${currCat}"_seed_"${currSeed}"_num_"${currNum}"_FrBase_"${currFrBase}"_num_"${currNum}".pattern_q

                        kuramoto=${src_dir}/res_"${currName}"_type_"${currCat}"_seed_"${currSeed}"_num_"${currNum}"_FrBase_"${currFrBase}"_num_"${currNum}".ku

                        echo "${pval_p}"
                        pval_line_no=$(cat ${pval_p} | wc -l)

                        check_line_no=$(cat ${pval_q} | wc -l)
                        if [ $pval_line_no -ne $check_line_no ]
                        then
                            echo "different line number in ${pval_q}"
                            exit
                        fi

                        check_line_no=$(cat ${pattern_p} | wc -l)
                        if [ $pval_line_no -ne $check_line_no ]
                        then
                            echo "different line number in ${pattern_p}"
                            exit
                        fi

                        check_line_no=$(cat ${pattern_q} | wc -l)
                        if [ $pval_line_no -ne $check_line_no ]
                        then
                            echo "different line number in ${pattern_q}"
                            exit
                        fi

                        check_line_no=$(cat ${kuramoto} | wc -l)
                        if [ $pval_line_no -ne $check_line_no ]
                        then
                            echo "different line number in ${kuramoto}"
                            exit
                        fi

                        if [ -f "${pval_p}" ];
                        then
                            if [ ! -s "${pval_p}" ];
                            then
                                echo -e "${RED}File is empty${NC}"
                                let "empty_test_no=empty_test_no+1"
                            fi
                            cat ${pattern_p} >> ${data_dir}/"$1"_"$2"_pattern_p.res
                            cat ${pval_p} >> ${data_dir}/"$1"_"$2"_pval_p.res
                            cat ${pattern_q} >> ${data_dir}/"$1"_"$2"_pattern_q.res
                            cat ${pval_q} >> ${data_dir}/"$1"_"$2"_pval_q.res
                            cat ${kuramoto} >> ${data_dir}/"$1"_"$2"_kuramoto.res
                            echo -e "${GREEN}OK${NC}"

                            let "exist_test_no=exist_test_no+1"
                        else
                            echo -e "${RED}File does not exist${NC}"
                            exit
                        fi
                        echo ""

                        let "test_no=test_no+1"
                    done
                done
            done
        done
    done
done

echo "empty_test_no: ${empty_test_no}, exist_test_no: ${exist_test_no}"

}

echo "cleanup data dir ..."
echo "rm -rf ${data_dir}"
rm -rf ${data_dir}
echo "mkdir ${data_dir}"
mkdir ${data_dir}
echo "DONE"
echo ""

save_coma_aggr_separ "plus" "sim"
#save_coma_aggr "plus" "fon"
save_coma_aggr_separ "minus" "sim"
#save_coma_aggr "minus" "fon"
