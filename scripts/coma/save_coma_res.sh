#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

data_dir=../../../data_coma_res

source Names.config

#echo "cleanup data dir ..."
#rm -rf ${data_dir}/*
#echo "rm -rf ${data_dir}/*"
#echo "DONE"
#echo ""

#declare -a Type=("sim" "fon")
declare -a Type=("sim")
declare -a Seed=("0" "1" "2")
declare -a FrBase=("4" "5" "6")
test_no=1
for currName in "${Name[@]}"
do
    for currType in "${Type[@]}"
    do
        for currSeed in "${Seed[@]}"
        do
            for currFrBase in "${FrBase[@]}"
            do
                echo "run #${test_no} mat_save_pat_pval( ${currName}, ${currType}, ${currSeed}, ${currFrBase})"

                res_out=${data_dir}/res_"${currName}"_type_"${currType}"_seed_"${currSeed}"_FrBase_"${currFrBase}".pval_p
                echo "${res_out}"

                if [ ! -f "${res_out}" ];
                then
                    echo "run MATLAB script ..."
                    if [[ "$OSTYPE" == "linux-gnu" ]]
                    then
                        echo "/usr/local/MATLAB/R2018a/bin/matlab -nodisplay -nodesktop -r \"mat_save_pat_pval($'${currName}','${currType}','${currSeed}','${currFrBase}')\""
                        /usr/local/MATLAB/R2018a/bin/matlab -nodisplay -nodesktop -r "mat_save_pat_pval('${currName}','${currType}','${currSeed}','${currFrBase}')"
                    else
                        echo "C:\Program Files\MATLAB\R2015b\bin\matlab.exe -nodisplay -nodesktop -logfile output.log -r \"mat_save_pat_pval($'${currName}','${currType}','${currSeed}','${currFrBase}')\""
                        "C:\Program Files\MATLAB\R2015b\bin\matlab.exe" -nodisplay -nodesktop -logfile output.log -r "mat_save_pat_pval('${currName}','${currType}','${currSeed}','${currFrBase}')"
                        sleep 3000s
                    fi
                    echo "DONE"
                fi

                echo -e "${GREEN}Run PASS${NC}"
                echo ""

                let "test_no=test_no+1"
            done
        done
    done
done
exit


