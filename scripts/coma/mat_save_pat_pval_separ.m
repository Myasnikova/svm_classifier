function mat_save_pat_pval_separ(name,stype,seed,FrBase,Num)

cd ../../../matlab_SPoC
startup_spoc
cd ../gcfd
startup_gcfd
back_coma(algo(front_coma_sim_separate(conf({'permute_elec','true'}),stype,{'Name',name,'permut_seed',seed,'load_concat_elec','true','FrBase',FrBase,'Num',Num})))
%back_coma(algo(front_coma(conf(),stype,{'Name',name,'permut_seed',seed,'load_concat_elec','true','FrBase',FrBase})))
exit

