#!/bin/bash

source ../Names.config
data_dir=/home/miaal165/data_coma_inSVM
create_dirs () {
    cur_dir=${data_dir}/$1
    mkdir ${cur_dir}
    for currName in "${Name[@]}"
    do
        mkdir ${cur_dir}/${currName}
    done
}

echo "cleanup data dir ..."
echo "rm -rf ${data_dir}"
rm -rf ${data_dir}
echo "mkdir ${data_dir}"
mkdir ${data_dir}
echo "DONE"
echo ""

echo "create dirs ..."
create_dirs "positive_negative"
echo "DONE"
echo ""

echo "data prep ..."
for currName in "${Name[@]}"
do
    if [[ "$OSTYPE" == "linux-gnu" ]]
    then
        echo "/usr/bin/matlab -nodisplay -nodesktop -r \"mat_gen_data_exclude('${currName}')\""
        /usr/bin/matlab -nodisplay -nodesktop -r "mat_gen_data_exclude('${currName}')"
    else
        echo "C:\Program Files\MATLAB\R2015b\bin\matlab.exe -nodisplay -nodesktop -logfile output.log -r \"mat_gen_data_exclude('${currName}')\""
        "C:\Program Files\MATLAB\R2015b\bin\matlab.exe" -nodisplay -nodesktop -logfile output.log -r "mat_gen_data_exclude('${currName}')"
    fi
done
echo "DONE"
echo ""

