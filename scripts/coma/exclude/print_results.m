fname_kn = 'exclude/baseline_kn.txt';
fname_kl = 'exclude/baseline_kl.txt';
fname_ln = 'exclude/baseline_ln.txt';

R_kn = load_results(fname_kn);
R_kl = load_results(fname_kl);
R_ln = load_results(fname_ln);

mean_kn = mean(R_kn);
mean_kl = mean(R_kl);
mean_ln = mean(R_ln);

median_kn = median(R_kn)
median_kl = median(R_kl)
median_ln = median(R_ln)

%median_acc = [median_kn median_kl median_ln];

medianB = [R_kn' R_kl' R_ln'];

figure; 
boxplot(medianB)

ylabel('Percentage of accuracy, %')

categ = {'KN','KL','LN'};
set(gca,'XTick',1:3,'XTickLabel',categ)
xlabel('Pairwise classification of spatial patterns')
