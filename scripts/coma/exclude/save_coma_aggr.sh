#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

src_dir=/home/miaal165/data_coma_res
data_dir=/home/miaal165/data_coma_aggr

save_coma_aggr_positive_negative () {

excludeName=$2
exclude=$3

if [ $1 = "plus" ];
then
    echo "Your choice is plus"
    declare -a Name=("syputdinov" "JLblCOB" "KBALLlHUHOB" "BAS" "zakharova" "ARSLANBEKOV" "ABDULATIPIV" "MAMEDOB" "BAHlEJLUI")
else [ $1 = "negative" ];
    echo "Your choice is minus"
    declare -a Name=("4EPHOPAEB" "CHULODIN" "4YP6AHOB" "AJLELLlUH" "baykov" "MAH9lXUH" "ERE" "VIN" "qPJLOPOBCKUI" "KUH" "HAT" "aleshin" "SHO")
fi

declare -a Seed=("0" "1" "2")
declare -a FrBase=("4" "5" "6")
test_no=1
exist_test_no=1
empty_test_no=1
pval_line_no=1
check_line_no=1

if [[ ${exclude} -ne 0 ]]
then
    for currName in "${Name[@]}"
    do
        if [ ${currName} == ${excludeName} ]
        then
            continue
        fi
        for currSeed in "${Seed[@]}"
        do
            for currFrBase in "${FrBase[@]}"
            do
                echo "file #${test_no}"

                pval_p=${src_dir}/res_"${currName}"_type_sim_seed_"${currSeed}"_FrBase_"${currFrBase}".pval_p
                pval_q=${src_dir}/res_"${currName}"_type_sim_seed_"${currSeed}"_FrBase_"${currFrBase}".pval_q
                pattern_p=${src_dir}/res_"${currName}"_type_sim_seed_"${currSeed}"_FrBase_"${currFrBase}".pattern_p
                pattern_q=${src_dir}/res_"${currName}"_type_sim_seed_"${currSeed}"_FrBase_"${currFrBase}".pattern_q
                kuramoto=${src_dir}/res_"${currName}"_type_sim_seed_"${currSeed}"_FrBase_"${currFrBase}".ku
                echo "${pval_p}"
                pval_line_no=$(cat ${pval_p} | wc -l)

                check_line_no=$(cat ${pval_q} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pval_q}"
                    exit
                fi

                check_line_no=$(cat ${pattern_p} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pattern_p}"
                    exit
                fi

                check_line_no=$(cat ${pattern_q} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pattern_q}"
                    exit
                fi

                check_line_no=$(cat ${kuramoto} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${kuramoto}"
                    exit
                fi

                if [ -f "${pval_p}" ];
                then
                    if [ ! -s "${pval_p}" ];
                    then
                        echo -e "${RED}File is empty${NC}"
                        let "empty_test_no=empty_test_no+1"
                    fi
                    cat ${pattern_p} >> ${data_dir}/${excludeName}/"$1"_train_pattern_p.res
                    cat ${pval_p} >> ${data_dir}/${excludeName}/"$1"_train_pval_p.res
                    cat ${pattern_q} >> ${data_dir}/${excludeName}/"$1"_train_pattern_q.res
                    cat ${pval_q} >> ${data_dir}/${excludeName}/"$1"_train_pval_q.res
                    cat ${kuramoto} >> ${data_dir}/${excludeName}/"$1"_train_kuramoto.res
                    echo -e "${GREEN}OK${NC}"

                    let "exist_test_no=exist_test_no+1"
                else
                    echo -e "${RED}File does not exist${NC}"
                fi
                echo ""

                let "test_no=test_no+1"
            done
        done
    done
else
    for currName in "${Name[@]}"
    do
        if [ ${currName} == ${excludeName} ]
        then
        for currSeed in "${Seed[@]}"
        do
            for currFrBase in "${FrBase[@]}"
            do
                echo "file #${test_no}"

                pval_p=${src_dir}/res_"${currName}"_type_sim_seed_"${currSeed}"_FrBase_"${currFrBase}".pval_p
                pval_q=${src_dir}/res_"${currName}"_type_sim_seed_"${currSeed}"_FrBase_"${currFrBase}".pval_q
                pattern_p=${src_dir}/res_"${currName}"_type_sim_seed_"${currSeed}"_FrBase_"${currFrBase}".pattern_p
                pattern_q=${src_dir}/res_"${currName}"_type_sim_seed_"${currSeed}"_FrBase_"${currFrBase}".pattern_q
                kuramoto=${src_dir}/res_"${currName}"_type_sim_seed_"${currSeed}"_FrBase_"${currFrBase}".ku
                echo "${pval_p}"
                pval_line_no=$(cat ${pval_p} | wc -l)

                check_line_no=$(cat ${pval_q} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pval_q}"
                    exit
                fi

                check_line_no=$(cat ${pattern_p} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pattern_p}"
                    exit
                fi

                check_line_no=$(cat ${pattern_q} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${pattern_q}"
                    exit
                fi

                check_line_no=$(cat ${kuramoto} | wc -l)
                if [ $pval_line_no -ne $check_line_no ]
                then
                    echo "different line number in ${kuramoto}"
                    exit
                fi

                if [ -f "${pval_p}" ];
                then
                    if [ ! -s "${pval_p}" ];
                    then
                        echo -e "${RED}File is empty${NC}"
                        let "empty_test_no=empty_test_no+1"
                    fi
                    cat ${pattern_p} >> ${data_dir}/${excludeName}/"$1"_test_pattern_p.res
                    cat ${pval_p} >> ${data_dir}/${excludeName}/"$1"_test_pval_p.res
                    cat ${pattern_q} >> ${data_dir}/${excludeName}/"$1"_test_pattern_q.res
                    cat ${pval_q} >> ${data_dir}/${excludeName}/"$1"_test_pval_q.res
                    cat ${kuramoto} >> ${data_dir}/${excludeName}/"$1"_test_kuramoto.res
                    echo -e "${GREEN}OK${NC}"

                    let "exist_test_no=exist_test_no+1"
                else
                    echo -e "${RED}File does not exist${NC}"
                fi
                echo ""

                let "test_no=test_no+1"
            done
        done
        fi
    done
fi

echo "empty_test_no: ${empty_test_no}, exist_test_no: ${exist_test_no}"

}

echo "cleanup data dir ..."
echo "rm -rf ${data_dir}"
rm -rf ${data_dir}
echo "mkdir ${data_dir}"
mkdir ${data_dir}
echo "DONE"
echo ""

declare -a Name_plus=("syputdinov" "JLblCOB" "KBALLlHUHOB" "BAS" "zakharova" "ARSLANBEKOV" "ABDULATIPIV" "MAMEDOB" "BAHlEJLUI")
declare -a Name_minus=("4EPHOPAEB" "CHULODIN" "4YP6AHOB" "AJLELLlUH" "baykov" "MAH9lXUH" "ERE" "VIN" "qPJLOPOBCKUI" "KUH" "HAT" "aleshin" "SHO")

for currNamePlus in "${Name_plus[@]}"
do
    mkdir ${data_dir}/${currNamePlus}
    save_coma_aggr_positive_negative "plus" ${currNamePlus} 0
    save_coma_aggr_positive_negative "plus" ${currNamePlus} 1
    save_coma_aggr_positive_negative "minus" ${currNamePlus} 1
done
for currNameMinus in "${Name_minus[@]}"
do
    mkdir ${data_dir}/${currNameMinus}
    save_coma_aggr_positive_negative "minus" ${currNameMinus} 0
    save_coma_aggr_positive_negative "minus" ${currNameMinus} 1
    save_coma_aggr_positive_negative "plus" ${currNameMinus} 1
done

