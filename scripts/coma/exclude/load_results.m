function R = load_results(fname)

N = 16;
for i = 0:N-1
    R(i+1) = csvread(fname,i,0,[i,0,i,0]);
end
