#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

data_dir=../../../data_coma_concat

#source Names_minus.config
source Names.config

#echo "cleanup data dir ..."
#rm -rf ${data_dir}/*
#echo "rm -rf ${data_dir}/*"
#echo "DONE"
#echo ""

#declare -a Type=("sim")
declare -a Type=("sim")
declare -a Seed=("0" "1" "2")
#declare -a Seed=("0")
test_no=1
for currName in "${Name[@]}"
do
    for currSeed in "${Seed[@]}"
    do
        for currType in "${Type[@]}"
        do
            echo "run #${test_no} mat_save_concat_elec(${currName}, ${currType}, ${currSeed})"

            concat_elec_out=${data_dir}/concat_elec_"${currName}"_type_"${currType}"_seed_"${currSeed}".elec
            echo "${concat_elec_out}"

            if [ ! -f "${concat_elec_out}" ];
            then
                echo "run MATLAB script ..."
                    if [[ "$OSTYPE" == "linux-gnu" ]]
                    then
                        echo "/usr/local/MATLAB/R2018a/bin/matlab -nodisplay -nodesktop -r \"mat_save_concat_elec('${currName}','${currType}','${currSeed}')\""
                        /usr/local/MATLAB/R2018a/bin/matlab -nodisplay -nodesktop -r "mat_save_concat_elec('${currName}','${currType}','${currSeed}')"
                    else
                        echo "C:\Program Files\MATLAB\R2015b\bin\matlab.exe -nodisplay -nodesktop -logfile output.log -r \"mat_save_pat_pval($'${currName}','${currType}','${currSeed}')\""
                        "C:\Program Files\MATLAB\R2015b\bin\matlab.exe" -nodisplay -nodesktop -logfile output.log -r "mat_save_concat_elec('${currName}','${currType}','${currSeed}')"
                    fi

                echo "DONE"
            fi

            echo -e "${GREEN}Run PASS${NC}"
            echo ""

            let "test_no=test_no+1"
        done
    done
done


