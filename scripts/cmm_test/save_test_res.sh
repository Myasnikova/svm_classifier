#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

# ---- concat<->res
data_dir=../../../data_test_res
#data_dir=../../../data_test_concat

#source Names_test.config

#echo "cleanup data dir ..."
#rm -rf ${data_dir}/*
#echo "rm -rf ${data_dir}/*"
#echo "DONE"
#echo ""

declare -a seed_pat_list=("2901" "1832" "893")
declare -a seed_src_sig_list=("149" "236" "484" "1121" "169" "383" "956" "1759" "4" "439" "1023")
declare -a seed_src_noise_list=("1236" "1035" "1570" "1049" "266" "1689" "257" "1076" "974" "178" "1105")

declare -a FrBase=("10")

test_no=1
for seed_pat in "${seed_pat_list[@]}"
do
    for seed_src_sig in "${seed_src_sig_list[@]}"
    do
        for seed_src_noise in "${seed_src_noise_list[@]}"
        do
            for currFrBase in "${FrBase[@]}"
            do
                echo "run #${test_no} mat_save_pat_pval_test( ${seed_pat}, ${seed_src_sig}, ${seed_src_noise}, ${currFrBase})"

                # ---- concat<->res
                #out=${data_dir}/concat_elec_"${seed_pat}"_"${seed_src_sig}"_"${seed_src_noise}".elec
                out=${data_dir}/res_"${seed_pat}"_"${seed_src_sig}"_"${seed_src_noise}"_FrBase_"${currFrBase}".pval_p
                echo "${out}"

                if [ ! -f "${out}" ];
                then
                    echo "run MATLAB script ..."
                    if [[ "$OSTYPE" == "linux-gnu" ]]
                    then
                        echo "/usr/local/MATLAB/R2018a/bin/matlab -nodisplay -nodesktop -r \"mat_save_pat_pval_test('${seed_pat}','${seed_src_sig}','${seed_src_noise}','${test_no}','${currFrBase}')\""
                        /usr/local/MATLAB/R2018a/bin/matlab -nodisplay -nodesktop -r "mat_save_pat_pval_test('${seed_pat}','${seed_src_sig}','${seed_src_noise}','${test_no}','${currFrBase}')"
                    else
                        echo "C:\Program Files\MATLAB\R2015b\bin\matlab.exe -nodisplay -nodesktop -logfile output.log -r \"mat_save_pat_pval_test($'${seed_pat}}','${seed_src_sig}','${seed_src_noise}','${test_no}','${currFrBase}')\""
                        "C:\Program Files\MATLAB\R2015b\bin\matlab.exe" -nodisplay -nodesktop -logfile output.log -r "mat_save_pat_pval_test('${seed_pat}','${seed_src_sig}','${seed_src_noise}','${test_no}','${currFrBase}')"
                        sleep 3000s
                    fi
                    echo "DONE"
                fi

                echo -e "${GREEN}Run PASS${NC}"
                echo ""

                let "test_no=test_no+1"
            done
        done
    done
done
exit
