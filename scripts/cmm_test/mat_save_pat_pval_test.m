function mat_save_pat_pval(seed_pat,seed_src_sig,seed_src_noise,mul,FrBase)

cd ../../../matlab_SPoC
startup_spoc
cd ../gcfd
startup_gcfd
m = 982451653
seed_pat_m = int2str(mod(str2num(seed_pat)*str2num(mul),m))
seed_src_sig_m = int2str(mod(str2num(seed_src_sig)*str2num(mul),m))
seed_src_noise_m = int2str(mod(str2num(seed_src_noise)*str2num(mul),m))

% ---- concat<->res
back_test(algo(front_test(conf({'permute_elec','true'}),{'seed_pat',seed_pat_m,'seed_src_sig',seed_src_sig_m,'seed_src_noise',seed_src_noise_m,'load_elec','true','FrBase',FrBase})))
%front_test(conf(),{'seed_pat',seed_pat_m,'seed_src_sig',seed_src_sig_m,'seed_src_noise',seed_src_noise_m,'save_elec','true','FrBase',FrBase,'single_sig','true'})

exit

