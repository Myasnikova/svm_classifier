function mat_save_pat_pval(name,stype,seed,FrBase)

cd ../../matlab_SPoC
startup_spoc
cd ../gcfd
startup_gcfd
back_cmm(algo(front_cmm(conf({'permute_elec','true'}),{'stype', 'cmm', 'Name',name,'permut_seed',seed,'load_elec','true','FrBase',FrBase})))
%back_coma(algo(front_coma(conf(),stype,{'Name',name,'permut_seed',seed,'load_concat_elec','true','FrBase',FrBase})))
exit
