function  data_gen(categ_compare,seed_from,seed_to)

for seed = seed_from:seed_to
    data_prep(categ_compare,seed);
    fprintf("%s seed #%d generated\n", categ_compare, seed);
end
