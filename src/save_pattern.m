function save_pattern(project,pattern_A,pattern_B,name,mode,y_A, y_B,rand_seed,categ_compare)

assert(strcmp(project, 'coma') || strcmp(project, 'categ'));

shuffle_test = true;

row_A = size(pattern_A,1);
yA = y_A*ones(row_A,1);
row_B = size(pattern_B,1);
yB = y_B*ones(row_B,1);

if isnan(pattern_A)
    patterns = pattern_B;
    y = yB;
elseif isnan(pattern_B)
    patterns = pattern_A;
    y = yA;
else
    patterns = vertcat(pattern_A,pattern_B);
    y = vertcat(yA, yB);
end

if shuffle_test
    row = size(patterns,1);
    row_rand = randperm(row);
    patterns = patterns(row_rand,:);
    y = y(row_rand);
end

assert(strcmp(mode,'train') || strcmp(mode,'test'));
if strcmp(project, 'categ')
    fname = strcat('../data_categ_inSVM/', categ_compare);
else
    fname = strcat('../data_coma_inSVM/', categ_compare);
end
fname = strcat(fname,'/');
if ~strcmp(name,'')
    fname = strcat(fname, name);
    fname = strcat(fname,'/');
end
fname = strcat(fname,'pattern_');
fname = strcat(fname, mode);
fname = strcat(fname,'_');
fname = strcat(fname,num2str(rand_seed));

fileID = fopen(fname,'w');
col = size(patterns,2);
for i = 1:row
    fprintf(fileID,'%d ',y(i));
    for j = 1:col
        fprintf(fileID,'%d:%.4f ',j,patterns(i,j));
    end
    fprintf(fileID,'\n');
end
fclose(fileID);
