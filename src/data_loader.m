function pattern = data_loader(project, categ, name, mode, type)

assert(type == 'p' || type == 'q' || type == 'k');

bad_ch_rm = false;
std_norm = false;

if bad_ch_rm
%bad_ch = [2,5,6,11,15,16];
    bad_ch = [18,19];
else
    bad_ch = [];
end

if strcmp(project, 'coma')
    pattern_dir_name = '../data_coma_aggr/';
else
    pattern_dir_name = '../data_categ_aggr/';
end

if ~strcmp(name,'')
    pattern_dir_name = strcat(pattern_dir_name, name);
    pattern_dir_name = strcat(pattern_dir_name,'/');
    categ_m = strcat(categ, '_');
    categ_m = strcat(categ_m, mode);
else
    assert(strcmp(mode,''));
    categ_m= categ;
end
if type == 'k'
    pattern_name = strcat(categ_m, '_kuramoto.res');
else
    pattern_name = strcat(categ_m, '_pattern_');
    pattern_name = strcat(pattern_name, type);
    pattern_name = strcat(pattern_name, '.res');
end
%disp(pattern_name);
pattern = dlmread(strcat(pattern_dir_name, pattern_name));

pval_p = strcat(pattern_dir_name, strcat(categ_m, '_pval_p.res'));
pval_q = strcat(pattern_dir_name, strcat(categ_m, '_pval_q.res'));
pval_p = dlmread(pval_p);
pval_q = dlmread(pval_q);

pat_num = size(pval_p,1);
assert(pat_num == size(pattern,1));
rowToDelete = [];
for i = 1:pat_num
    if pval_p(i) > 0.05 || pval_q(i) > 0.05
       rowToDelete = [rowToDelete,i];
    end
end
pattern(rowToDelete,:) = [];

if std_norm
    std_n = std(pattern);
    col_num = size(pattern,2);
    for i = 1: col_num
        pattern(:,i) = pattern(:,i)/std_n(i);
    end
end

pattern(:,bad_ch) = [];
