function [pattern_train, pattern_test] = split_pattern(pattern)

train_part = 0.8;

row = size(pattern,1);
row_rand = randperm(row);
pattern = pattern(row_rand,:);

spltk = floor(row * train_part);
pattern_train = pattern(1:spltk,:);
pattern_test = pattern(spltk+1:end,:);
