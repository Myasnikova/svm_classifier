function data_prep(categ_compare,rand_seed)

rng(rand_seed);

[pattern_train_k, pattern_test_k] = create_pattern('k','','',false);
[pattern_train_l, pattern_test_l] = create_pattern('l','','',false);
[pattern_train_n, pattern_test_n] = create_pattern('n','','',false);

if strcmp('kn', categ_compare)
    pattern_train_A = pattern_train_k;
    pattern_train_B = pattern_train_n;
    pattern_test_A  = pattern_test_k;
    pattern_test_B  = pattern_test_n;
elseif strcmp('ln', categ_compare)
    pattern_train_A = pattern_train_l;
    pattern_train_B = pattern_train_n;
    pattern_test_A  = pattern_test_l;
    pattern_test_B  = pattern_test_n;
elseif strcmp('kl', categ_compare)
    pattern_train_A = pattern_train_k;
    pattern_train_B = pattern_train_l;
    pattern_test_A  = pattern_test_k;
    pattern_test_B  = pattern_test_l;
else
    assert(0);
end
    
y_A = -1;
y_B = 1;
save_pattern(pattern_train_A,pattern_train_B,'','train',y_A,y_B,rand_seed,categ_compare);
save_pattern(pattern_test_A, pattern_test_B, '','test', y_A,y_B,rand_seed,categ_compare);
