dir = '/home/sasha/Desktop/GCFD/data/categ/';
dst_dir = '/home/sasha/Desktop/GCFD/data/categ_perm/';

names = {'ATANOVMS/','HISAMOVAAI/','KORKINAEP/','SOKOLOVASV/','LASUNINNV/',...
         'MARKINA/','PETRUNINAEL/','PETRUNINAO/','SIMAKOVA/','USOVAAA/',...
         'NEZHDANOVKS/','MIASNIKOVAAS/','KULIKAYU/','ALEXEEVSA/',...
         'ALIEVAA/','LOBODAAV/','MISHINSA/','MIASNIKOVAAS/'};
for j=1:size(names,2)
    name = names{j};
    [f1, f2] = categ_perm_list(dir, name);
    N = size(f1,1);
    for i = 1:N
        categ_swap(dir, dst_dir, name, f1{i}, f2{i});
    end
end
