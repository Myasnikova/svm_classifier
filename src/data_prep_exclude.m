function data_prep_exclude(project,categ_compare,rand_seed,name,mode)

assert(strcmp(project,'categ') || strcmp(project,'coma'));
assert(strcmp(mode,'train') || strcmp(mode,'test'));

rng(rand_seed);

if strcmp(project, 'categ')
    [pattern_k,~] = create_pattern('k', name, mode, true);
    [pattern_l,~] = create_pattern('l', name, mode, true);
    [pattern_n,~] = create_pattern('n', name, mode, true);

    if strcmp('kn', categ_compare)
        pattern_A = pattern_k;
        pattern_B = pattern_n;
    elseif strcmp('ln', categ_compare)
        pattern_A = pattern_l;
        pattern_B = pattern_n;
    elseif strcmp('kl', categ_compare)
        pattern_A = pattern_k;
        pattern_B = pattern_l;
    else
        assert(0);
    end
else
    if strcmp(mode, 'train')
        [pattern_minus,~] = create_pattern(project,'minus', name, mode, true);
        [pattern_plus,~] = create_pattern(project,'plus', name, mode, true);
        pattern_A = pattern_minus;
        pattern_B = pattern_plus;
    else
        if strcmp(name, 'syputdinov')  || strcmp(name, 'JLblCOB') || ...
           strcmp(name, 'KBALLlHUHOB') || strcmp(name, 'BAS') || ...
           strcmp(name, 'zakharova')   || strcmp(name, 'ARSLANBEKOV') || ...
           strcmp(name, 'ABDULATIPIV') || strcmp(name, 'MAMEDOB') || ...
           strcmp(name, 'BAHlEJLUI')
           [pattern_plus,~] = create_pattern(project,'plus', name, mode, true);
           pattern_A = NaN;
           pattern_B = pattern_plus;
        else
           [pattern_minus,~] = create_pattern(project,'minus', name, mode, true);
           pattern_A = pattern_minus;
           pattern_B = NaN;
        end
    end
end
    
y_A = -1;
y_B = 1;
save_pattern(project,pattern_A,pattern_B,name,mode,y_A,y_B,rand_seed,categ_compare);
