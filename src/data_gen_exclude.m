function  data_gen_exclude(project,categ_compare,seed_from,seed_to,name,mode)

for seed = seed_from:seed_to
    data_prep_exclude(project,categ_compare,seed,name,mode);
    fprintf('project: %s, %s seed #%d generated\n', project, categ_compare, seed);
end
