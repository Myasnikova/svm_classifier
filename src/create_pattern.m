function [pattern_train, pattern_test] = create_pattern(project,categ,name,mode,exclude_test)

scale = true;
if exclude_test
    scale = false;
end

pattern_p = data_loader(project,categ,name,mode,'p');
pattern_q = data_loader(project,categ,name,mode,'q');
kuramoto  = data_loader(project,categ,name,mode,'k');

if strcmp(project, 'coma')
    pattern = [kuramoto pattern_p(:,3:end) pattern_q(:,3:end)];
else
    pattern = [kuramoto pattern_p pattern_q];
end

if scale
    pattern = scale_pattern(project,pattern);
end

if exclude_test
    pattern_train = pattern;
    pattern_test = NaN;
    return
end

[pattern_train, pattern_test] = split_pattern(pattern);
