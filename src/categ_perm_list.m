function [f1, f2] = categ_perm_list(dir, name)

rng(size(name,2))

cur_dir = strcat(dir,name);
l = ls(cur_dir);
s = strsplit(l);

N = size(s,2)-1;
f = strings(N,1);
taskperm = randperm(N);
for i=1:N
    f{taskperm(i)} = s{i};
end

M = floor(N/2);
f1 = strings(M,1);
for i=1:M
    f1{i} = f{i};
end

f2 = strings(M,1);
for i=1:M
    f2{i} = f{M+i};
end

[f1 f2]
