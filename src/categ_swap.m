function categ_swap(src_dir, dst_dir, name, fname1, fname2)

src_curr_dir = strcat(src_dir,name);
dst_curr_dir = strcat(dst_dir,name);

src_file1 = strcat(src_curr_dir, fname1);
src_file2 = strcat(src_curr_dir, fname2);

dst_file1 = strcat(dst_curr_dir, fname1);
dst_file2 = strcat(dst_curr_dir, fname2);

copyfile(src_file1, dst_file2);
copyfile(src_file2, dst_file1);
