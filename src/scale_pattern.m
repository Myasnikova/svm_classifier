function pattern_scaled = scale_pattern(project,pattern)

assert(strcmp(project, 'coma') || strcmp(project, 'categ'));

max_pat = max(pattern);
row = size(pattern,1);
col = size(pattern,2);

if strcmp(project,"categ")
    assert(col == 39);
else
    assert(col == 35);
end

one_pat = ones(row,col);
for i = 1:col
    max_pat_mat(:,i) =  max_pat(i)*one_pat(:,i);
end
pattern_scaled = pattern./max_pat_mat;
