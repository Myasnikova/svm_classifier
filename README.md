# svm_classifier

an EEG project for classifying topographies of functional interactions when performing categorization tasks

# TODO for ML:

single hidden layer - and more

CV for:
1) polynomial degree
2) lambda (regularization)

Dignostics: learning curves(m)
bais vs variance

# Hints and tips for ML:
There are two main options to address the issue of overfitting:

1) Reduce the number of features:
Manually select which features to keep.
Use a model selection algorithm (studied later in the course).

2) Regularization
Keep all the features, but reduce the magnitude of parameters theta.
Regularization works well when we have a lot of slightly useful features.

# Putting it Together
First, pick a network architecture;
choose the layout of your neural network, including how many hidden units in each layer and how many layers in total you want to have.

Number of input units = dimension of features
Number of output units = number of classes
Number of hidden units per layer = usually more the better (must balance with cost of computation as it increases with more hidden units)
Defaults: 1 hidden layer. If you have more than 1 hidden layer, then it is recommended that you have the same number of units in every hidden layer.

# Training a Neural Network
Randomly initialize the weights
Implement forward propagation to get h
Implement the cost function
Implement backpropagation to compute partial derivatives
Use gradient checking to confirm that your backpropagation works. Then disable gradient checking.
Use gradient descent or a built-in optimization function to minimize the cost function with the weights in theta.

# Once we have done some trouble shooting for errors in our predictions by:
Getting more training examples
Trying smaller sets of features
Trying additional features
Trying polynomial features
Increasing or decreasing lambda

A hypothesis may have a low error for the training examples but still be inaccurate (because of overfitting).
Thus, to evaluate a hypothesis, given a dataset of training examples, we can split up the data into two sets:
a training set and a test set. Typically, the training set consists of 70 % of your data and the test set is the remaining 30 %.

Training set: 60%
Cross validation set: 20%
Test set: 20%

High bias (underfitting): both J_train and J_CV will be high.
High variance (overfitting): J_train will be low and J_CV will be much greater than J_train.

# The recommended approach to solving machine learning problems is to:

Start with a simple algorithm, implement it quickly, and test it early on your cross validation data.
Plot learning curves to decide if more data, more features, etc. are likely to help.
Manually examine the errors on examples in the cross validation set and try to spot a trend where most of the errors were made.

